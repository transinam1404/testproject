using System.Linq.Expressions;
using System.Text;
using FasCon.Apis.Patterns;
using PetaPoco;

namespace FasCon.Apis.EntityFrameworks
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        public GenericRepository(Database context)
        {
            this._context = context;
        }

        public void Delete(string CompanyId, string BranchId, object id)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("where 1 = 1");
            if (!string.IsNullOrEmpty(CompanyId))
            {
                stringBuilder.AppendFormat(" and CompanyId = '{0}'", CompanyId);
            }
            if (!string.IsNullOrEmpty(BranchId))
            {
                stringBuilder.AppendFormat(" and BranchId = '{0}'", BranchId);
            }
            if (id != null)
            {
                stringBuilder.Append(" and Id = @0");
            }
            if (id != null)
            {
                this._context.Delete<TEntity>(stringBuilder.ToString(), new object[]
                {
                    id
                });
            }
        }

        public void Delete(TEntity entity)
        {
            if (entity != null)
            {
                this._context.Delete(entity);
            }
        }

        public void Insert(TEntity entity)
        {
            if (entity != null)
            {
                this._context.Insert(entity);
            }
        }

        public void Update(TEntity entity)
        {
            if (entity != null)
            {
                this._context.Update(entity);
            }
        }

        public IEnumerable<TEntity> Query(string CompanyId, string BranchId)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("where 1 = 1");
            if (!string.IsNullOrEmpty(CompanyId))
            {
                stringBuilder.AppendFormat(" and CompanyId = '{0}'", CompanyId);
            }
            if (!string.IsNullOrEmpty(BranchId))
            {
                stringBuilder.AppendFormat(" and BranchId = '{0}'", BranchId);
            }
            return this._context.Query<TEntity>(stringBuilder.ToString(), new object[0]).ToArray<TEntity>();
        }

        public IEnumerable<TEntity> Query(string CompanyId, string BranchId, Expression<Func<TEntity, bool>> predicate)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("where 1 = 1");
            if (!string.IsNullOrEmpty(CompanyId))
            {
                stringBuilder.AppendFormat(" and CompanyId = '{0}'", CompanyId);
            }
            if (!string.IsNullOrEmpty(BranchId))
            {
                stringBuilder.AppendFormat(" and BranchId = '{0}'", BranchId);
            }
            return this._context.Query<TEntity>(stringBuilder.ToString(), new object[0]).AsQueryable<TEntity>().Where(predicate).ToArray<TEntity>();
        }

        public IEnumerable<TEntity> Query(string sql, params object[] args)
        {
            return this._context.Query<TEntity>(sql, args).ToArray<TEntity>();
        }

        public int Execute(string sql, params object[] args)
        {
            if (this._context.Execute(sql, args) == 1)
            {
                return 0;
            }
            return 9;
        }

        public TEntity FirstOrDefault(string sql, params object[] args)
        {
            return this._context.FirstOrDefault<TEntity>(sql, args);
        }

        public TEntity FirstOrDefault(string CompanyId, string BranchId, Expression<Func<TEntity, bool>> predicate)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("where 1 = 1");
            if (!string.IsNullOrEmpty(CompanyId))
            {
                stringBuilder.AppendFormat(" and CompanyId = '{0}'", CompanyId);
            }
            if (!string.IsNullOrEmpty(BranchId))
            {
                stringBuilder.AppendFormat(" and BranchId = '{0}'", BranchId);
            }
            return this._context.Query<TEntity>(stringBuilder.ToString(), new object[0]).AsQueryable<TEntity>().FirstOrDefault(predicate);
        }

        public TEntity FirstOrDefault(string CompanyId, string BranchId, object id)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("where 1 = 1");
            if (!string.IsNullOrEmpty(CompanyId))
            {
                stringBuilder.AppendFormat(" and CompanyId = '{0}'", CompanyId);
            }
            if (!string.IsNullOrEmpty(BranchId))
            {
                stringBuilder.AppendFormat(" and BranchId = '{0}'", BranchId);
            }
            if (id != null)
            {
                stringBuilder.Append("and Id = @0");
            }
            return this._context.Query<TEntity>(stringBuilder.ToString(), new object[]
            {
                id
            }).FirstOrDefault<TEntity>();
        }

        internal Database _context;
    }
}
