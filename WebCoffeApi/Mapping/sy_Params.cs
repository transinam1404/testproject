﻿
// Type: FasCon.Apis.Mapping.sy_Params




using PetaPoco;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class sy_Params
  {
    public long Id { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string Value { get; set; }

    public string CompanyId { get; set; }

    public string BranchId { get; set; }
  }
}
