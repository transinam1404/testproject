﻿
// Type: FasCon.Apis.Mapping.M07_Promotion_KM_TangHang




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class M07_Promotion_KM_TangHang
  {
    public long Id { get; set; }

    public string DocInfoId { get; set; }

    public string DocumentNo { get; set; }

    public int GroupId { get; set; }

    public long ItemId { get; set; }

    public long UnitId { get; set; }

    public Decimal Qty { get; set; }

    public short IsKhuyenMai { get; set; }

    public short IsDeleted { get; set; }

    public string CompanyId { get; set; }

    public short Canceled { get; set; }
  }
}
