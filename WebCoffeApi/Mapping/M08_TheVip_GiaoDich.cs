﻿
// Type: FasCon.Apis.Mapping.M08_TheVip_GiaoDich




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class M08_TheVip_GiaoDich
  {
    public long Id { get; set; }

    public string InfoId { get; set; }

    public string DocumentNo { get; set; }

    public long VipId { get; set; }

    public DateTime DocumentDate { get; set; }

    public DateTime PostingDate { get; set; }

    public Decimal Info_TotalAmount { get; set; }

    public Decimal MucChietKhau { get; set; }

    public Decimal HeSoQuyDoiDiem { get; set; }

    public Decimal DiemTichLuy { get; set; }

    public Decimal DiemTichLuyLamTron { get; set; }

    public short DocumentType { get; set; }

    public string Note { get; set; }

    public string CompanyId { get; set; }

    public string BranchId { get; set; }
  }
}
