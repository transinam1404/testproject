﻿
// Type: FasCon.Apis.Mapping.T03_Sales_Invoice_TienGio




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class T03_Sales_Invoice_TienGio
  {
    public long Id { get; set; }

    public string InfoId { get; set; }

    public long AreaId { get; set; }

    public long TimeId { get; set; }

    public long TableId { get; set; }

    public Decimal Price { get; set; }

    public DateTime FromTime { get; set; }

    public short FromHour { get; set; }

    public short FromMinute { get; set; }

    public DateTime ToTime { get; set; }

    public short ToHour { get; set; }

    public short ToMinute { get; set; }

    public Decimal TotalTime { get; set; }

    public Decimal Amount { get; set; }

    public Decimal Discount { get; set; }

    public Decimal DisAmount { get; set; }

    public Decimal TotalAmount { get; set; }

    public int TongSoPhut { get; set; }

    public string SoGio { get; set; }

    public string CompanyId { get; set; }

    public string BranchId { get; set; }

    public short IsDetail { get; set; }
  }
}
