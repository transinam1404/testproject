﻿
// Type: FasCon.Apis.Mapping.M08_ThietLapTheVip




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class M08_ThietLapTheVip
  {
    public long Id { get; set; }

    public Decimal HeSoQuyDoiDiem { get; set; }

    public Decimal HanMucDoiDiemThuong { get; set; }

    public string CompanyId { get; set; }
  }
}
