using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class M04_Size_TraSua
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
    }
}
