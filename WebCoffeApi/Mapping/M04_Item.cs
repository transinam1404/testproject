﻿
// Type: FasCon.Apis.Mapping.M04_Item




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class M04_Item
  {
    public long Id { get; set; }

    public string Code { get; set; }

    public string Barcode { get; set; }

    public string Name { get; set; }

    public string Search_Name { get; set; }

    public long? UnitBasic { get; set; }

    public string Note { get; set; }

    public int? Posting_GroupId { get; set; }

    public long? Item_CategoryId { get; set; }

    public Decimal? Min_Quantity { get; set; }

    public Decimal? Max_Quantity { get; set; }

    public short? Non_Stock { get; set; }

    public short? Inv_Value_Zero { get; set; }

    public short? IsUsed { get; set; }

    public short? Item_Type { get; set; }

    public short? AddOn { get; set; }

    public short? ShowChooseAddOn { get; set; }

    public short? AllowSelectedSize { get; set; }

    public short AllowPrintTopping { get; set; }

    public int? VAT_ProdGroupId { get; set; }

    public short? ManufacturerId { get; set; }

    public short? DueDate { get; set; }

    public string Picture { get; set; }

    public short? StatusId { get; set; }

    public string CompanyId { get; set; }

    public short? IsDeleted { get; set; }

    public string ImgPath { get; set; }

    public Decimal CostPrice { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public string UpdatedBy { get; set; }

    public DateTime? ApprovedDate { get; set; }

    public string ApprovedBy { get; set; }

    public long SellLocationId { get; set; }
  }
}
