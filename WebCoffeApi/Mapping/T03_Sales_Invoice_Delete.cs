﻿
// Type: FasCon.Apis.Mapping.T03_Sales_Invoice_Delete




using System;


namespace FasCon.Apis.Mapping
{
  public class T03_Sales_Invoice_Delete
  {
    public string Id { get; set; }

    public string CompanyId { get; set; }

    public string BranchId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string Note { get; set; }

    public string DocumentNo { get; set; }

    public Decimal Info_TotalAmount { get; set; }
  }
}
