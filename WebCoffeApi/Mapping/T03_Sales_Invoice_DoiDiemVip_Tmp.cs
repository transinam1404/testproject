﻿
// Type: FasCon.Apis.Mapping.T03_Sales_Invoice_DoiDiemVip_Tmp




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class T03_Sales_Invoice_DoiDiemVip_Tmp
  {
    public long Id { get; set; }

    public string InfoId { get; set; }

    public long MaTheVipId { get; set; }

    public Decimal HanMucDoiDiemThuong { get; set; }

    public string CompanyId { get; set; }

    public string BranchId { get; set; }
  }
}
