﻿
// Type: FasCon.Apis.Mapping.T03_Sales_Invoice_TienKetThucCa




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id")]
  public class T03_Sales_Invoice_TienKetThucCa
  {
    public long Id { get; set; }

    public long UserId { get; set; }

    public string UserName { get; set; }

    public int To200D { get; set; }

    public int To500D { get; set; }

    public int To1000D { get; set; }

    public int To2000D { get; set; }

    public int To5000D { get; set; }

    public int To10000D { get; set; }

    public int To20000D { get; set; }

    public int To50000D { get; set; }

    public int To100000D { get; set; }

    public int To200000D { get; set; }

    public int To500000D { get; set; }

    public Decimal TongTienDauCa { get; set; }

    public Decimal TongTienKetThucCa { get; set; }

    public DateTime Ngay { get; set; }

    public DateTime GioKetThucCa { get; set; }

    public string CompanyId { get; set; }

    public string BranchId { get; set; }

    public long TienDauCaId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public string UpdatedBy { get; set; }
  }
}
