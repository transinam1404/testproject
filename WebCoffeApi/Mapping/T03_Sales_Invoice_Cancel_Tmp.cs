﻿
// Type: FasCon.Apis.Mapping.T03_Sales_Invoice_Cancel_Tmp




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class T03_Sales_Invoice_Cancel_Tmp
  {
    public long Id { get; set; }

    public string IdTmp { get; set; }

    public string InfoId { get; set; }

    public string DocumentNo { get; set; }

    public short? AddOn { get; set; }

    public long? ParentItemId { get; set; }

    public long? ItemId { get; set; }

    public Decimal? Quantity { get; set; }

    public long? UomId { get; set; }

    public string Size_TraSua { get; set; }

    public Decimal? Uom_PerQty { get; set; }

    public Decimal? Qty_To_Receive { get; set; }

    public Decimal? Qty_Received { get; set; }

    public Decimal? Qty_Remain { get; set; }

    public Decimal? UnitPrice { get; set; }

    public Decimal? UnitPrice_LCY { get; set; }

    public Decimal? VAT_Pecent { get; set; }

    public Decimal? Amount { get; set; }

    public Decimal? Amount_LCY { get; set; }

    public Decimal? DisCount01 { get; set; }

    public Decimal? Disc_Amount01 { get; set; }

    public Decimal? DisCount02 { get; set; }

    public Decimal? Disc_Amount02 { get; set; }

    public Decimal? Total_DiscAmount { get; set; }

    public Decimal? VAT_DiscAmount { get; set; }

    public Decimal? VatAmount { get; set; }

    public Decimal? VAT_Amount_LCY { get; set; }

    public Decimal? TotalAmount { get; set; }

    public Decimal? TotalAmount_LCY { get; set; }

    public Decimal? UnitCost { get; set; }

    public Decimal? AmountCost { get; set; }

    public long? VAT_BussGroupId { get; set; }

    public long? VAT_ProdGroupId { get; set; }

    public short Dis_AmountEdit { get; set; }

    public short StatusId { get; set; }

    public short IsPrintReturn { get; set; }

    public string Note { get; set; }

    public long? LocationId { get; set; }

    public string BranchId { get; set; }

    public string CompanyId { get; set; }

    public string DocumentNo_Ref { get; set; }

    public string NVTraMon { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public string UpdatedBy { get; set; }

    public long? Id_local { get; set; }
  }
}
