using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class M03_Sale_Table
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? AreaId { get; set; }
        public short? Arrangement { get; set; }
        public short? IsUsed { get; set; }
        public string Note { get; set; }
        public short? StatusId { get; set; }
        public string BranchId { get; set; }
        public string CompanyId { get; set; }
        public short? IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }
    }
}
