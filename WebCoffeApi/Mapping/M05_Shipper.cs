﻿
// Type: FasCon.Apis.Mapping.M05_Shipper




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class M05_Shipper
  {
    public long Id { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public long GroupShipperId { get; set; }

    public string Note { get; set; }

    public short? StatusId { get; set; }

    public short IsUsed { get; set; }

    public string BranchId { get; set; }

    public string CompanyId { get; set; }

    public short? IsDeleted { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public string UpdatedBy { get; set; }
  }
}
