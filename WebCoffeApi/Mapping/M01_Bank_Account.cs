using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
	[PrimaryKey("Id", AutoIncrement = false)]
	public class M01_Bank_Account
	{
		public long Id { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public string Search_Name { get; set; }
		public int? Posting_GroupId { get; set; }
		public int? CurrencyId { get; set; }
		public DateTime? PostingFrom { get; set; }
		public DateTime? PostingTo { get; set; }
		public decimal? Min_Balance { get; set; }
		public short? IsUsed { get; set; }
		public string Note { get; set; }
		public string BranchId { get; set; }
		public string CompanyId { get; set; }
		public int? StatusId { get; set; }
		public short? IsDeleted { get; set; }
		public DateTime? CreatedDate { get; set; }
		public string CreatedBy { get; set; }
		public DateTime? UpdatedDate { get; set; }
		public string UpdatedBy { get; set; }
		public DateTime? ApprovedDate { get; set; }
		public string ApprovedBy { get; set; }
	}
}
