﻿
// Type: FasCon.Apis.Mapping.M07_Promotion_Invoice




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class M07_Promotion_Invoice
  {
    public long Id { get; set; }

    public string DocInfoId { get; set; }

    public string DocumentNo { get; set; }

    public int? PromotionLevel { get; set; }

    public Decimal? FromAmount { get; set; }

    public Decimal? ToAmount { get; set; }

    public Decimal? Discount { get; set; }

    public Decimal? DiscAmount { get; set; }

    public Decimal? DiscAmountEdit { get; set; }

    public string Note { get; set; }

    public string CompanyId { get; set; }

    public short? Canceled { get; set; }

    public short? IsDeleted { get; set; }
  }
}
