using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
	[PrimaryKey("Id")]
	public class M03_Price_Detail_POS
	{
		public long Id { get; set; }
		public long InfoId { get; set; }
		public long ItemId { get; set; }
		public string Barcode { get; set; }
		public long UnitId { get; set; }
		public long ColorId { get; set; }
		public long SizeId { get; set; }
		public decimal Price { get; set; }
		public string BranchId { get; set; }
		public string CompanyId { get; set; }
		public short? IsDeleted { get; set; }
		public DateTime? CreatedDate { get; set; }
		public string CreatedBy { get; set; }
		public DateTime? UpdatedDate { get; set; }
		public string UpdatedBy { get; set; }
	}
}
