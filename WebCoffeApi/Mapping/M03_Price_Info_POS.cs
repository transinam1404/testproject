using System;
using PetaPoco;

namespace FasCon.Apis.Mapping
{
	[PrimaryKey("Id")]
	public class M03_Price_Info_POS
	{
		public long Id { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public DateTime? from_datetime { get; set; }
		public DateTime? to_datetime { get; set; }
		public short? PriceType { get; set; }
		public short? Used { get; set; }
		public string Note { get; set; }
		public string CompanyId { get; set; }
		public string BranchId { get; set; }
		public short? IsDeleted { get; set; }
		public short? Canceled { get; set; }
		public DateTime? CreatedDate { get; set; }
		public string CreatedBy { get; set; }
		public DateTime? UpdatedDate { get; set; }
		public string UpdatedBy { get; set; }
	}
}
