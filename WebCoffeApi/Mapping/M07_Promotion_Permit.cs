﻿
// Type: FasCon.Apis.Mapping.M07_Promotion_Permit




using PetaPoco;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class M07_Promotion_Permit
  {
    public long Id { get; set; }

    public string DocInfoId { get; set; }

    public string BranchId { get; set; }

    public string CompanyId { get; set; }

    public short? Allowed { get; set; }

    public short? IsDeleted { get; set; }
  }
}
