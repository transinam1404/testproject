﻿
// Type: FasCon.Apis.Mapping.S01_Location_Store




using PetaPoco;
using System;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class S01_Location_Store
  {
    public long Id { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string Address { get; set; }

    public string PhoneNo { get; set; }

    public string FaxNo { get; set; }

    public DateTime? PostingFrom { get; set; }

    public DateTime? PostingTo { get; set; }

    public short? Inv_Value_Zero { get; set; }

    public short? LocationType { get; set; }

    public string Packing_User { get; set; }

    public string BranchId { get; set; }

    public string CompanyId { get; set; }

    public short? IsUsed { get; set; }

    public short? StatusId { get; set; }

    public short? IsDeleted { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public string UpdatedBy { get; set; }

    public DateTime? ApprovedDate { get; set; }

    public string ApprovedBy { get; set; }
  }
}
