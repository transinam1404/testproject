﻿
// Type: FasCon.Apis.Mapping.M08_KhaiBaoMucChietKhau




using PetaPoco;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class M08_KhaiBaoMucChietKhau
  {
    public long Id { get; set; }

    public string Code { get; set; }

    public double TuGiaTri { get; set; }

    public double DenCanGiaTri { get; set; }

    public long LoaiThe { get; set; }

    public double MucChietKhau { get; set; }

    public string GhiChu { get; set; }

    public string CompanyId { get; set; }

    public short IsDeleted { get; set; }
  }
}
