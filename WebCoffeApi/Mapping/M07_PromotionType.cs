﻿
// Type: FasCon.Apis.Mapping.M07_PromotionType




using PetaPoco;


namespace FasCon.Apis.Mapping
{
  [PrimaryKey("Id", AutoIncrement = false)]
  public class M07_PromotionType
  {
    public int Id { get; set; }

    public string Description { get; set; }

    public short? IsVisible { get; set; }

    public string CompanyId { get; set; }
  }
}
