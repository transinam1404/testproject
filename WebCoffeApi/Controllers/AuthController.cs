using System;
using System.Data.SqlClient;
using FasCon.Apis.Common;
using FasCon.Apis.Models;
using FasCon.Apis.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FasCon.Apis.Controller
{
    //Nam fixed this controller.
    [ApiController]
    [Route("[controller]")]
    public class AuthController :ControllerBase
    {
        public AuthController()
        {
            this._authService = new AuthService();
        }
        
        //Note userName and password get from database.
        [Route("api/Auth/CheckLogin")]
        [HttpPost]
        public ResponseMessageModels CheckLogin(UserLoginModels model)
        {
            int num = 9;
            model.BranchCode = "sgu";
            return new ResponseMessageModels
            {
                Data = this._authService.CheckLogin(model, out num),
                Status = new StatusCodeModels
                {
                    StatusCode = num,
                    MessageId = CommonData.DisplayMessage(num),
                    Version = "v1"
                }
            };
        }
      

        private IAuthService _authService;
    }
}
