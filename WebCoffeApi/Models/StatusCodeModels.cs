﻿
// Type: FasCon.Apis.Models.StatusCodeModels





namespace FasCon.Apis.Models
{
  public class StatusCodeModels
  {
    public int StatusCode { get; set; }

    public string MessageId { get; set; }

    public string Version { get; set; }
  }
}
