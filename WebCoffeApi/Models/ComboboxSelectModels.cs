﻿
// Type: FasCon.Apis.Models.ComboboxSelectModels




using System;


namespace FasCon.Apis.Models
{
  public class ComboboxSelectModels
  {
    public object ID { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string NameEN { get; set; }

    public string Field3 { get; set; }

    public string Field4 { get; set; }

    public string Field5 { get; set; }

    public object Field6 { get; set; }

    public object Field7 { get; set; }

    public object Field8 { get; set; }

    public object Field9 { get; set; }

    public object Field10 { get; set; }

    public object Field11 { get; set; }

    public object Field12 { get; set; }

    public string ImgPath { get; set; }

    public Decimal ItemTotalSales { get; set; }

    public int isChecked { get; set; }

    public Decimal QtyOrder { get; set; }

    public object CostPrice { get; set; }

    public string UnitCode { get; set; }

    public Decimal UnitPrice { get; set; }

    public string Barcode { get; set; }

    public string Note { get; set; }

    public Decimal GiaBanLe { get; set; }

    public long SellLocationId { get; set; }

    public long ItemId { get; set; }

    public string Size_TraSua { get; set; }

    public Decimal ItemVATPercent { get; set; }

    public int AllowVAT_Item { get; set; }

    public ComboboxSelectModels()
    {
      this.ID = (object) 0;
      this.Code = string.Empty;
      this.Name = string.Empty;
      this.NameEN = string.Empty;
      this.Field3 = string.Empty;
      this.Field4 = string.Empty;
      this.Field5 = string.Empty;
      this.Field6 = (object) string.Empty;
      this.Field7 = (object) string.Empty;
      this.Field8 = (object) string.Empty;
      this.Field9 = (object) string.Empty;
      this.Field10 = (object) string.Empty;
      this.Field11 = (object) string.Empty;
      this.Field12 = (object) string.Empty;
      this.UnitCode = string.Empty;
      this.Note = string.Empty;
      this.isChecked = 0;
      this.QtyOrder = 0M;
      this.Barcode = string.Empty;
      this.ItemTotalSales = 0M;
    }
  }
}
