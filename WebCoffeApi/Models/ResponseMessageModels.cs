﻿
// Type: FasCon.Apis.Models.ResponseMessageModels





namespace FasCon.Apis.Models
{
  public class ResponseMessageModels
  {
    public object DataConfig { get; set; }

    public object Data { get; set; }

    public object Data2 { get; set; }

    public object Data3 { get; set; }

    public object Data4 { get; set; }

    public object Data5 { get; set; }

    public object Data6 { get; set; }

    public object Data7 { get; set; }

    public object Data8 { get; set; }

    public object Data9 { get; set; }

    public object Data10 { get; set; }

    public object Data11 { get; set; }

    public object Data12 { get; set; }

    public object Data13 { get; set; }

    public object Data14 { get; set; }

    public object Data15 { get; set; }

    public object Data16 { get; set; }

    public object Data17 { get; set; }

    public object Data18 { get; set; }

    public object Data19 { get; set; }

    public object Data20 { get; set; }

    public object Data21 { get; set; }

    public object Data22 { get; set; }

    public StatusCodeModels Status { get; set; }

    public object RoleUser { get; set; }

    public object ArrHoaDon { get; set; }

    public object ArrTonKho { get; set; }

    public int isCheckTienDauCa { get; set; }

    public byte[] byteData { get; set; }

    public byte[] byteData2 { get; set; }

    public byte[] byteData3 { get; set; }

    public byte[] byteData4 { get; set; }

    public byte[] byteData5 { get; set; }

    public object LicenseList { get; set; }

    public object Params { get; set; }
  }
}
