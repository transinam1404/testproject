﻿
// Type: FasCon.Apis.Common.CommonData




using System.Runtime.InteropServices;


namespace FasCon.Apis.Common
{
  public class CommonData
  {
    private static string[] arrDayOfWeeks = new string[7]
    {
      "Sun",
      "Mon",
      "Tue",
      "Wed",
      "Thu",
      "Fri",
      "Sat"
    };

    public static string DisplayMessage(int iReturnCode)
    {
      string str = string.Empty;
      switch (iReturnCode)
      {
        case 0:
          str = "CMN_MSG_SAVE_SUCESSFULLY";
          break;
        case 1:
          str = "CMN_MSG_ACCESS_DENIED";
          break;
        case 2:
          str = "CMN_MSG_INVALID_HOST";
          break;
        case 3:
          str = "CMN_MSG_INVALID_DATABASE";
          break;
        case 4:
          str = "CMN_MSG_LOST_CONNECTION";
          break;
        case 5:
          str = "CMN_MSG_DUPLICATE_DATA";
          break;
        case 6:
          str = "CMN_MSG_FORGEIGN_NOT_EXIST";
          break;
        case 7:
          str = "CMN_MSG_FORGEIGN_KEY_VIOLATION";
          break;
        case 8:
          str = "CMN_MSG_DATA_NOT_FOUND";
          break;
        case 9:
          str = "CMN_MSG_EXCEPTION_OCCURED";
          break;
        case 10:
          str = "CMN_MSG_DELETE_CURRENT_USER";
          break;
        case 11:
          str = "CMN_MSG_DUPLICATE_DOCUMENT_NO";
          break;
        case 12:
          str = "CMN_MSG_NOT_PERMISSION";
          break;
      }
      return str;
    }

    public static string getDayOfWeeks(int dayofweek)
    {
      string empty = string.Empty;
      return CommonData.arrDayOfWeeks[dayofweek];
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct DbReturnCode
    {
      public const int Succeed = 0;
      public const int AccessDenied = 1;
      public const int InvalidHost = 2;
      public const int InvalidDatabase = 3;
      public const int LostConnection = 4;
      public const int DuplicateKey = 5;
      public const int ForgeignKeyNotExist = 6;
      public const int ForeignKeyViolation = 7;
      public const int DataNotFound = 8;
      public const int ExceptionOccured = 9;
      public const int DeleteCurrentUser = 10;
      public const int DuplicateDocumentNo = 11;
      public const int UserNotPermissionAction = 12;
      public const int FileNotExist = 13;
      public const int NotLicense = 14;
      public const int ServiceUnavailable = 503;
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct LoaiHinhKinhDoanh
    {
      public const string NhaHang_Cafe = "001";
      public const string TraSua = "002";
      public const string BanLe = "003";
      public const string Karaoke = "004";
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct MySqlErrorNumber
    {
      public const int AccessDenied = 1045;
      public const int DuplicateKey = 1062;
      public const int ForgeignKeyNotExist = 1452;
      public const int ForgeignKeyViolation = 1451;
      public const int InvalidDatabase = 1049;
      public const int InvalidHost = 2005;
      public const int LostConnection = 1042;
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct DefaultValue
    {
      public const string LocationDefault = "LocationDefault";
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct EPriceNo
    {
      public const string Price1 = "Price1";
      public const string Price2 = "Price2";
      public const string Price3 = "Price3";
      public const string Price4 = "Price4";
      public const string Price5 = "Price5";
      public const string Price6 = "Price6";
      public const string Price7 = "Price7";
      public const string Price8 = "Price8";
      public const string Price9 = "Price9";
      public const string Price10 = "Price10";
      public const string Price11 = "Price11";
      public const string Price12 = "Price12";
      public const string Price13 = "Price13";
      public const string Price14 = "Price14";
      public const string Price15 = "Price15";
      public const string Price16 = "Price16";
      public const string Price17 = "Price17";
      public const string Price18 = "Price18";
      public const string Price19 = "Price19";
      public const string Price20 = "Price20";
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct CounterPrimaryKey
    {
      public const string PhieuNhapKho = "PNK";
      public const string PhieuXuatKho = "PXK";
      public const string PhieuChuyenKho = "PCK";
      public const string HoaDonBanSi = "HDS";
      public const string HoaDonBanLe = "HDL";
      public const string HoaDonBanTraHang = "PNT";
      public const string DonDatHangBan = "DHB";
      public const string PhieuChiTien = "PC";
      public const string PhieuThuTien = "PT";
      public const string PromotionDeal = "CTKM";
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct StatusType
    {
      public const int Active = 1;
      public const int InActive = 0;
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct ScreenCode
    {
      public const string ItemCategory = "ItemCategory";
      public const string Item = "Item";
      public const string Cost = "Cost";
      public const string Customer = "Customer";
      public const string CustomerCategory = "CustomerCategory";
      public const string Department = "Department";
      public const string Employee = "Employee";
      public const string LocationStore = "LocationStore";
      public const string PriceList = "PriceList";
      public const string RelationPrice = "RelationPrice";
      public const string SaleArea = "SaleArea";
      public const string SaleTable = "SaleTable";
      public const string Supplier = "Supplier";
      public const string SupplierCategory = "SupplierCategory";
      public const string TimeOfSale = "TimeOfSale";
      public const string UnitOfMeasure = "UnitOfMeasure";
      public const string Restaurant = "Restaurant";
      public const string RestaurantList = "RestaurantList";
      public const string Promotion = "Promotion";
      public const string StockIn = "StockIn";
      public const string StockOut = "StockOut";
      public const string StockTransfer = "StockTransfer";
      public const string InvDinhMuc = "InvDinhMuc";
      public const string Sales_DoanhSoNhomTheoNhanVien = "Sales_DoanhSoNhomTheoNhanVien";
      public const string Sales_ChiTietBanHang_TheoNhomHang = "Sales_ChiTietBanHang_TheoNhomHang";
      public const string Sales_ChiTietBanHang_TheoKhuVuc = "Sales_ChiTietBanHang_TheoKhuVuc";
      public const string Inv_BC_TonKho = "Inv_BC_TonKho";
      public const string Inv_BC_NhapXuatTon = "Inv_BC_NhapXuatTon";
      public const string ScreenPOSLe = "ScreenPOSLe";
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct ActionCode
    {
      public const string View = "View";
      public const string Search = "Search";
      public const string Save = "Save";
      public const string Delete = "Delete";
      public const string Import = "Import";
      public const string Export = "Export";
      public const string ThanhLyData = "ThanhLyData";
      public const string ResetKho = "ResetKho";
      public const string UploadReport = "UploadReport";
      public const string InCheBien = "InCheBien";
      public const string InHoaDon = "InHoaDon";
      public const string ChuyenBan = "ChuyenBan";
      public const string GopBan = "GopBan";
      public const string EditGioVao = "EditGioVao";
      public const string ChoPhepSuaGiaBan = "ChoPhepSuaGiaBan";
      public const string TraMonKhiDaInHoaDon = "TraMonKhiDaInHoaDon";
      public const string ChangePassword = "ChangePassword";
      public const string ResetPassword = "ResetPassword";
      public const string CreateAuto = "CreateAuto";
      public const string GhiNhanTinhGio = "GhiNhanTinhGio";
      public const string ChoPhepTraMonSauInHoaDon = "ChoPhepTraMonSauInHoaDon";
      public const string ChoPhepTraMonSauInCheBien = "ChoPhepTraMonSauInCheBien";
      public const string InCheBienTruocInHoaDon = "InCheBienTruocInHoaDon";
      public const string SuaCKSauKhiInHD = "SuaCKSauKhiInHD";
      public const string EditPostingDate = "EditPostingDate";
      public const string OnlyPrintAndSave = "OnlyPrintAndSave";
      public const string LuuKhongXacNhanInHoaDon = "LuuKhongXacNhanInHoaDon";
      public const string ChoPhepXoaHangHoaChuaInHoaDon = "ChoPhepXoaHangHoaChuaInHoaDon";
      public const string ChoPhepSync_UpdateVer = "ChoPhepSync_UpdateVer";
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct MessageNotRole
    {
      public const string Search = "CMN_MSG_NOT_PERMISSION_SEARCH";
      public const string Edit = "CMN_MSG_NOT_PERMISSION_EDIT";
      public const string Delete = "CMN_MSG_NOT_PERMISSION_DELETE";
      public const string Save = "CMN_MSG_NOT_PERMISSION_SAVE";
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct FunctionCode
    {
      public const string DanhMuc = "DanhMuc";
      public const string HeThong = "HeThong";
      public const string HangHoa = "HangHoa";
      public const string BanHang = "BanHang";
      public const string MuaHang = "MuaHang";
      public const string ThuChi = "ThuChi";
      public const string KhoHang = "KhoHang";
      public const string KhuyenMai = "KhuyenMai";
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct iPromotionType
    {
      public const int KMHangHoa = 1;
      public const int KMHoaDon = 2;
      public const int KMTangHang = 3;
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct LocationSystemRes
    {
      public const string KBH = "KBH";
      public const string KDL = "KDL";
      public const string KHOBAR = "KHOBAR";
      public const string KHOBEP = "KHOBEP";
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct ItemTienGio
    {
      public const string TienGio = "TienGio";
      public const string TienGio2 = "TienGio2";
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct PriceType
    {
      public const short BangGiaLe = 1;
      public const short BangGiaSi = 0;
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct Inv_DocumentType
    {
      public const int All_Input = -2;
      public const int All_Output = -1;
      public const int NhapKhoNoiBo = 0;
      public const int XuatKhoNoiBo = 1;
      public const int NhapDieuChinhTangKho = 2;
      public const int XuatDieuChinhGiamKho = 3;
      public const int NhapThanhPham = 4;
      public const int XuatTieuHao = 5;
      public const int NhapPheLieu = 6;
      public const int XuatPheLieu = 7;
      public const int NhapMua = 8;
      public const int XuatBan = 9;
      public const int NhapTraHang_KH = 10;
      public const int XuatTraHang_NCC = 11;
      public const int NhapKhoKhacChiNhanh = 12;
      public const int XuatKhoKhacChiNhanh = 13;
      public const int XuatHaoHut = 15;
      public const int XuatSuDung = 17;
    }
  }
}
