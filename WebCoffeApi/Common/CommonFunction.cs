using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Net.Http;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FasCon.Apis.Models;
using Microsoft.CSharp.RuntimeBinder;

namespace FasCon.Apis.Common
{

    public class CommonFunction
	{
		 
		 
		[DllImport("wininet.dll")]
		private static extern bool InternetGetConnectedState(out int Description, int ReservedValue);

		 
		public static bool CheckInternet()
		{
			int num;
			return CommonFunction.InternetGetConnectedState(out num, 0);
		}

	 
		public static object DataTableToJSON(DataTable table)
		{
			List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
			foreach (object obj in table.Rows)
			{
				DataRow dataRow = (DataRow)obj;
				Dictionary<string, object> dictionary = new Dictionary<string, object>();
				foreach (object obj2 in table.Columns)
				{
					DataColumn dataColumn = (DataColumn)obj2;
					dictionary[dataColumn.ColumnName] = Convert.ToString(dataRow[dataColumn]);
				}
				list.Add(dictionary);
			}
			//return new JavaScriptSerializer
			//{
			//	MaxJsonLength = 50000000
			//}.Serialize(list);
			return null;
		}


		public static byte[] Serialize(object O, bool IsZip = false)
		{
			if (IsZip)
			{
				return CommonFunction.ZipBuffer(CommonFunction.Serialize(O, false));
			}
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			MemoryStream memoryStream = new MemoryStream();
			binaryFormatter.Serialize(memoryStream, O);
			return memoryStream.ToArray();
		}

	 
		public static byte[] ZipBuffer(byte[] buff)
		{
			MemoryStream memoryStream = new MemoryStream();
			GZipStream gzipStream = new GZipStream(memoryStream, CompressionMode.Compress);
			gzipStream.Write(buff, 0, buff.Length);
			gzipStream.Close();
			return memoryStream.ToArray();
		}

		 
		public static byte[] ZipBuffer(string s)
		{
			return CommonFunction.ZipBuffer(new UnicodeEncoding().GetBytes(s));
		}

		 
		public static object Deserialize(byte[] RawByte, bool IsZip = false)
		{
			if (IsZip)
			{
				return CommonFunction.Deserialize(CommonFunction.UnZipBuffer(RawByte, null), false);
			}
			MemoryStream serializationStream = new MemoryStream(RawByte);
			return new BinaryFormatter().Deserialize(serializationStream);
		}

	 
		public static string UnZipBuffer(byte[] buff, bool ConvertToString)
		{
			return new UnicodeEncoding().GetString(CommonFunction.UnZipBuffer(buff, null));
		}

		 
		public static byte[] UnZipBuffer(byte[] buff, MemoryStream msResult = null)
		{
			GZipStream gzipStream = new GZipStream(new MemoryStream(buff, 0, buff.Length)
			{
				Position = 0L
			}, CompressionMode.Decompress);
			if (msResult == null)
			{
				msResult = new MemoryStream();
			}
			CommonFunction.CopyStream(gzipStream, msResult);
			gzipStream.Close();
			msResult.Position = 0L;
			return msResult.ToArray();
		}

		 
		public static void CopyStream(Stream sStream, Stream dStream)
		{
			byte[] array = new byte[2049];
			for (;;)
			{
				int num = sStream.Read(array, 0, array.Length);
				if (num <= 0)
				{
					break;
				}
				dStream.Write(array, 0, num);
			}
		}

	 
		public static string DateForSQL(DateTime vDate)
		{
			return string.Format("{0:MM/dd/yyyy}", vDate);
		}

	 
		public static decimal LamTronSo(decimal Number, int SoKySo)
		{
			decimal result;
			try
			{
				result = (decimal)(Math.Round(Convert.ToDouble(Number) / Math.Pow(10.0, (double)SoKySo) + 0.1, 0) * Math.Pow(10.0, (double)SoKySo));
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return result;
		}

	 
		public static string DocSoRaChu(ulong num)
		{
			string[] array = new string[]
			{
				"Không",
				"Một",
				"Hai",
				"Ba",
				"Bốn",
				"Năm",
				"Sáu",
				"Bảy",
				"Tám",
				"Chín"
			};
			string text = num.ToString() ?? "";
			if (text.Length > 12)
			{
				return "";
			}
			string text2 = "";
			int[] array2 = new int[text.Length];
			for (int i = 0; i <= text.Length - 1; i++)
			{
				array2[i] = int.Parse(text.Substring(i, 1));
			}
			switch (array2.Length)
			{
			case 1:
				text2 = array[array2[0]];
				break;
			case 2:
				if (array2[0] == 1)
				{
					text2 += " Mười";
				}
				else
				{
					text2 = text2 + " " + array[array2[0]] + " Mươi";
				}
				if (array2[1] != 0)
				{
					if (array2[1] == 1)
					{
						if (array2[0] != 1)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[1]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[1]];
					}
				}
				break;
			case 3:
				text2 = text2 + array[array2[0]] + " Trăm";
				if (array2[1] == 0 && array2[2] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[1] != 0)
				{
					if (array2[1] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[1]] + " Mươi";
					}
				}
				if (array2[2] != 0)
				{
					if (array2[2] == 1)
					{
						if (array2[1] != 1 && array2[1] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[2]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[2]];
					}
				}
				break;
			case 4:
				text2 = text2 + array[array2[0]] + " Nghìn";
				if (array2[1] != 0 || array2[2] != 0 || array2[3] != 0)
				{
					text2 = text2 + " " + array[array2[1]] + " Trăm";
				}
				if (array2[2] == 0 && array2[3] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[2] != 0)
				{
					if (array2[2] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[2]] + " Mươi";
					}
				}
				if (array2[3] != 0)
				{
					if (array2[3] == 1)
					{
						if (array2[2] != 1 && array2[2] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[3]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[3]];
					}
				}
				break;
			case 5:
				if (array2[0] == 1)
				{
					text2 += " Mười";
				}
				else
				{
					text2 = text2 + " " + array[array2[0]] + " Mươi";
				}
				if (array2[1] != 0)
				{
					if (array2[1] == 1)
					{
						if (array2[0] != 1)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[1]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[1]];
					}
				}
				text2 += " Nghìn";
				if (array2[2] != 0 || array2[3] != 0 || array2[4] != 0)
				{
					text2 = text2 + " " + array[array2[2]] + " Trăm";
				}
				if (array2[3] == 0 && array2[4] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[3] != 0)
				{
					if (array2[3] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[3]] + " Mươi";
					}
				}
				if (array2[4] != 0)
				{
					if (array2[4] == 1)
					{
						if (array2[3] != 1 && array2[3] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[4]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[4]];
					}
				}
				break;
			case 6:
				text2 = text2 + array[array2[0]] + " Trăm";
				if (array2[1] == 0 && array2[2] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[1] != 0)
				{
					if (array2[1] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[1]] + " Mươi";
					}
				}
				if (array2[2] != 0)
				{
					if (array2[2] == 1)
					{
						if (array2[1] != 1 && array2[1] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[2]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[2]];
					}
				}
				text2 += " Nghìn";
				if (array2[3] != 0 || array2[4] != 0 || array2[5] != 0)
				{
					text2 = text2 + " " + array[array2[3]] + " Trăm";
				}
				if (array2[4] == 0 && array2[5] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[4] != 0)
				{
					if (array2[4] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[4]] + " Mươi";
					}
				}
				if (array2[5] != 0)
				{
					if (array2[5] == 1)
					{
						if (array2[4] != 1 && array2[4] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[5]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[5]];
					}
				}
				break;
			case 7:
				text2 = text2 + array[array2[0]] + " Triệu";
				if (array2[1] != 0 || array2[2] != 0 || array2[3] != 0)
				{
					text2 = text2 + " " + array[array2[1]] + " Trăm";
				}
				if (array2[2] == 0 && array2[3] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[2] != 0)
				{
					if (array2[2] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[2]] + " Mươi";
					}
				}
				if (array2[3] != 0)
				{
					if (array2[3] == 1)
					{
						if (array2[2] != 1 && array2[2] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[3]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[3]];
					}
				}
				if (array2[1] != 0 || array2[2] != 0 || array2[3] != 0)
				{
					text2 += " Nghìn";
				}
				if (array2[4] != 0 || array2[5] != 0 || array2[6] != 0)
				{
					text2 = text2 + " " + array[array2[4]] + " Trăm";
				}
				if (array2[5] == 0 && array2[6] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[5] != 0)
				{
					if (array2[5] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[5]] + " Mươi";
					}
				}
				if (array2[6] != 0)
				{
					if (array2[6] == 1)
					{
						if (array2[5] != 1 && array2[5] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[6]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[6]];
					}
				}
				break;
			case 8:
				if (array2[0] == 1)
				{
					text2 += " Mười";
				}
				else
				{
					text2 = text2 + " " + array[array2[0]] + " Mươi";
				}
				if (array2[1] != 0)
				{
					if (array2[1] == 1)
					{
						if (array2[0] != 1)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[1]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[1]];
					}
				}
				text2 += " Triệu";
				if (array2[2] != 0 || array2[3] != 0 || array2[4] != 0)
				{
					text2 = text2 + " " + array[array2[2]] + " Trăm";
				}
				if (array2[3] == 0 && array2[4] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[3] != 0)
				{
					if (array2[3] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[3]] + " Mươi";
					}
				}
				if (array2[4] != 0)
				{
					if (array2[4] == 1)
					{
						if (array2[3] != 1 && array2[3] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[4]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[4]];
					}
				}
				if (array2[2] != 0 || array2[3] != 0 || array2[4] != 0)
				{
					text2 += " Nghìn";
				}
				if (array2[5] != 0 || array2[6] != 0 || array2[7] != 0)
				{
					text2 = text2 + " " + array[array2[5]] + " Trăm";
				}
				if (array2[6] == 0 && array2[7] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[6] != 0)
				{
					if (array2[6] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[6]] + " Mươi";
					}
				}
				if (array2[7] != 0)
				{
					if (array2[7] == 1)
					{
						if (array2[6] != 1 && array2[6] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[7]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[7]];
					}
				}
				break;
			case 9:
				text2 = text2 + array[array2[0]] + " Trăm";
				if (array2[1] == 0 && array2[2] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[1] != 0)
				{
					if (array2[1] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[1]] + " Mươi";
					}
				}
				if (array2[2] != 0)
				{
					if (array2[2] == 1)
					{
						if (array2[1] != 1 && array2[1] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[2]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[2]];
					}
				}
				text2 += " Triệu";
				if (array2[3] != 0 || array2[4] != 0 || array2[5] != 0)
				{
					text2 = text2 + " " + array[array2[3]] + " Trăm";
				}
				if (array2[4] == 0 && array2[5] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[4] != 0)
				{
					if (array2[4] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[4]] + " Mươi";
					}
				}
				if (array2[5] != 0)
				{
					if (array2[5] == 1)
					{
						if (array2[4] != 1 && array2[4] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[5]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[5]];
					}
				}
				if (array2[3] != 0 || array2[4] != 0 || array2[5] != 0)
				{
					text2 += " Nghìn";
				}
				if (array2[6] != 0 || array2[7] != 0 || array2[8] != 0)
				{
					text2 = text2 + " " + array[array2[6]] + " Trăm";
				}
				if (array2[7] == 0 && array2[8] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[7] != 0)
				{
					if (array2[7] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[7]] + " Mươi";
					}
				}
				if (array2[8] != 0)
				{
					if (array2[8] == 1)
					{
						if (array2[7] != 1 && array2[7] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[8]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[8]];
					}
				}
				break;
			case 10:
				text2 = text2 + array[array2[0]] + " Tỷ ";
				if (array2[1] != 0 || array2[2] != 0 || array2[3] != 0)
				{
					text2 = text2 + " " + array[array2[1]] + " Trăm";
				}
				if (array2[2] == 0 && array2[3] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[2] != 0)
				{
					if (array2[2] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[2]] + " Mươi";
					}
				}
				if (array2[3] != 0)
				{
					if (array2[3] == 1)
					{
						if (array2[2] != 1 && array2[2] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[3]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[3]];
					}
				}
				if (array2[1] != 0 || array2[2] != 0 || array2[3] != 0)
				{
					text2 += " Triệu";
				}
				if (array2[4] != 0 || array2[5] != 0 || array2[6] != 0)
				{
					text2 = text2 + " " + array[array2[4]] + " Trăm";
				}
				if (array2[5] == 0 && array2[6] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[5] != 0)
				{
					if (array2[5] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[5]] + " Mươi";
					}
				}
				if (array2[6] != 0)
				{
					if (array2[6] == 1)
					{
						if (array2[5] != 1 && array2[5] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[6]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[6]];
					}
				}
				if (array2[4] != 0 || array2[5] != 0 || array2[6] != 0)
				{
					text2 += " Nghìn";
				}
				if (array2[7] != 0 || array2[8] != 0 || array2[9] != 0)
				{
					text2 = text2 + " " + array[array2[7]] + " Trăm";
				}
				if (array2[8] == 0 && array2[9] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[8] != 0)
				{
					if (array2[8] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[8]] + " Mươi";
					}
				}
				if (array2[9] != 0)
				{
					if (array2[9] == 1)
					{
						if (array2[8] != 1 && array2[8] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[9]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[9]];
					}
				}
				break;
			case 11:
				if (array2[0] == 1)
				{
					text2 += " Mười";
				}
				else
				{
					text2 = text2 + " " + array[array2[0]] + " Mươi";
				}
				if (array2[1] != 0)
				{
					if (array2[1] == 1)
					{
						if (array2[0] != 1)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[1]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[1]];
					}
				}
				text2 += " Tỷ";
				if (array2[2] != 0 || array2[3] != 0 || array2[4] != 0)
				{
					text2 = text2 + " " + array[array2[2]] + " Trăm";
				}
				if (array2[3] == 0 && array2[4] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[3] != 0)
				{
					if (array2[3] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[3]] + " Mươi";
					}
				}
				if (array2[4] != 0)
				{
					if (array2[4] == 1)
					{
						if (array2[3] != 1 && array2[3] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[4]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[4]];
					}
				}
				if (array2[2] != 0 || array2[3] != 0 || array2[4] != 0)
				{
					text2 += " Triệu";
				}
				if (array2[5] != 0 || array2[6] != 0 || array2[7] != 0)
				{
					text2 = text2 + " " + array[array2[5]] + " Trăm";
				}
				if (array2[6] == 0 && array2[7] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[6] != 0)
				{
					if (array2[6] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[6]] + " Mươi";
					}
				}
				if (array2[7] != 0)
				{
					if (array2[7] == 1)
					{
						if (array2[6] != 1 && array2[6] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[7]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[7]];
					}
				}
				if (array2[5] != 0 || array2[6] != 0 || array2[7] != 0)
				{
					text2 += " Nghìn";
				}
				if (array2[8] != 0 || array2[9] != 0 || array2[10] != 0)
				{
					text2 = text2 + " " + array[array2[8]] + " Trăm";
				}
				if (array2[9] == 0 && array2[10] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[9] != 0)
				{
					if (array2[9] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[9]] + " Mươi";
					}
				}
				if (array2[10] != 0)
				{
					if (array2[10] == 1)
					{
						if (array2[9] != 1 && array2[9] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[10]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[10]];
					}
				}
				break;
			case 12:
				text2 = text2 + array[array2[0]] + " Trăm";
				if (array2[1] == 0 && array2[2] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[1] != 0)
				{
					if (array2[1] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[1]] + " Mươi";
					}
				}
				if (array2[2] != 0)
				{
					if (array2[2] == 1)
					{
						if (array2[1] != 1 && array2[1] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[2]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[2]];
					}
				}
				text2 += " Tỷ";
				if (array2[3] != 0 || array2[4] != 0 || array2[5] != 0)
				{
					text2 = text2 + " " + array[array2[3]] + " Trăm";
				}
				if (array2[4] == 0 && array2[5] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[4] != 0)
				{
					if (array2[4] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[4]] + " Mươi";
					}
				}
				if (array2[5] != 0)
				{
					if (array2[5] == 1)
					{
						if (array2[4] != 1 && array2[4] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[5]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[5]];
					}
				}
				if (array2[3] != 0 || array2[4] != 0 || array2[5] != 0)
				{
					text2 += " Triệu";
				}
				if (array2[6] != 0 || array2[7] != 0 || array2[8] != 0)
				{
					text2 = text2 + " " + array[array2[6]] + " Trăm";
				}
				if (array2[7] == 0 && array2[8] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[7] != 0)
				{
					if (array2[7] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[7]] + " Mươi";
					}
				}
				if (array2[8] != 0)
				{
					if (array2[8] == 1)
					{
						if (array2[7] != 1 && array2[7] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[7]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[8]];
					}
				}
				if (array2[6] != 0 || array2[7] != 0 || array2[8] != 0)
				{
					text2 += " Nghìn";
				}
				if (array2[9] != 0 || array2[10] != 0 || array2[11] != 0)
				{
					text2 = text2 + " " + array[array2[9]] + " Trăm";
				}
				if (array2[10] == 0 && array2[11] != 0)
				{
					text2 += " Lẻ";
				}
				else if (array2[10] != 0)
				{
					if (array2[10] == 1)
					{
						text2 += " Mười";
					}
					else
					{
						text2 = text2 + " " + array[array2[10]] + " Mươi";
					}
				}
				if (array2[11] != 0)
				{
					if (array2[11] == 1)
					{
						if (array2[10] != 1 && array2[10] != 0)
						{
							text2 += " Mốt";
						}
						else
						{
							text2 = text2 + " " + array[array2[11]];
						}
					}
					else
					{
						text2 = text2 + " " + array[array2[11]];
					}
				}
				break;
			}
			return text2;
		}

		 
		public static DataTable ToDataTable<T>(List<T> items)
		{
			DataTable dataTable = new DataTable(typeof(T).Name);
			PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public);
			foreach (PropertyInfo propertyInfo in properties)
			{
				dataTable.Columns.Add(propertyInfo.Name);
			}
			foreach (T t in items)
			{
				object[] array2 = new object[properties.Length];
				for (int j = 0; j < properties.Length; j++)
				{
					array2[j] = properties[j].GetValue(t, null);
				}
				dataTable.Rows.Add(array2);
			}
			return dataTable;
		}

	 
		public static string EncryptSHA(string cleanString)
		{
			byte[] bytes = new UnicodeEncoding().GetBytes(cleanString);
			return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(bytes));
		}

 

		 
		public static string Chang_AV(string ip_str_change)
		{
			Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
			string input = ip_str_change.Normalize(NormalizationForm.FormD);
			return regex.Replace(input, string.Empty).Replace('đ', 'd').Replace('Đ', 'D');
		}

	 
	 
		public static Color colorBanTrong
		{
			get
			{
				return Color.FromArgb(240, 240, 240);
			}
		}

		 
		public static Color colorChonMon
		{
			get
			{
				return Color.FromArgb(33, 166, 222);
			}
		}

	 
		public static Color colorInCheBien
		{
			get
			{
				return Color.FromArgb(222, 89, 33);
			}
		}

		 
		public static Color colorInHoaDon
		{
			get
			{
				return Color.FromArgb(223, 33, 167);
			}
		}

		 
		private HttpClient client = new HttpClient();

public static TokenModels tokenStatus = null;

public static string webServiceApi = string.Empty;

public static string webServiceApiLocalHost = string.Empty;

public static bool isInternet = true;

public static string strPrinterInNhan = string.Empty;

public static string currentVersion = "1.0.0.0";

public static string curVersionDatabase = string.Empty;

public static bool isServerOffline = false;

public static int Port = 8090;

public static bool isExistDataLocal = false;

public static int LoginType = 0;

public struct ApiName
{
    public const string ApiRestaurant = "api/Restaurant/";
    public const string ApiRestaurantList = "api/RestaurantList/";
    public const string ApiCombobox = "api/Combobox/";
    public const string ApiSync = "api/SyncDataMaster/";
    public const string SaleArea = "api/SaleArea/";
    public const string RelationItemCategoryPrinter = "api/RelationItemCategoryPrinter/";
    public const string Customer = "api/Customer/";
    public const string Item = "api/Item/";
    public const string SaleReports = "api/SaleReports/";
    public const string BGLePOS = "api/BGLePOS/";
    public const string SaleReturn = "api/SaleReturn/";
    public const string Auth = "api/Auth/";
    public const string ApiCheckConnect = "api/CheckConnect/";
}
		public struct FunctionName
{
    public const string InitCustomerSearch = "InitCustomerSearch";
    public const string reloadMasterPlus = "reloadMasterPlus";
    public const string InitItemDetail = "InitItemDetail";
    public const string GetMaxCode = "GetMaxCode";
    public const string SaveExpress = "SaveExpress";
    public const string GetDetail = "detail";
    public const string GetDataRePrint = "GetDataRePrint";
    public const string InitHoaDonBanLe = "InitHoaDonBanLe";
    public const string InitHoaDonBanLeByteArray = "InitHoaDonBanLeByteArray";
    public const string InitHoaDonBanLe2 = "InitHoaDonBanLe2";
    public const string InitRestaurantListSearch = "InitRestaurantListSearch";
    public const string GetNumberDeviceConnect = "GetNumberDeviceConnect";
    public const string InitTouchScreen = "InitTouchScreen";
    public const string InitTouchScreen2 = "InitTouchScreen2";
    public const string BaoCaoKetCa = "BaoCaoKetCa";
    public const string GetListTableHasCustomer = "GetListTableHasCustomer";
    public const string GetDataByKhuVucBan = "GetDataByKhuVucBan";
    public const string GetDataByKhuVucBanByteArray = "GetDataByKhuVucBanByteArray";
    public const string GetPriceItem = "GetPriceItem";
    public const string SaveInvoiceTmp = "SaveInvoiceTmp";
    public const string SaveInvoiceTmpByteArray = "SaveInvoiceTmpByteArray";
    public const string InHoaDon = "InHoaDon";
    public const string InHoaDonFromTouchWin = "InHoaDonFromTouchWin";
    public const string SaveInvoice = "SaveInvoice";
    public const string UpdateStatusTable = "UpdateStatusTable";
    public const string GetDataInCheBien = "GetDataInCheBien";
    public const string GetDataInTraMon = "GetDataInTraMon";
    public const string UpdateTrangThaiInTraMon = "UpdateTrangThaiInTraMon";
    public const string GetTableList_MergeTransfer = "GetTableList_MergeTransfer";
    public const string Execute_Transfer = "Execute_Transfer";
    public const string Execute_Merge = "Execute_Merge";
    public const string Execute_SplitTable = "Execute_SplitTable";
    public const string SelectedAddon = "SelectedAddon";
    public const string GetlistInNhanTraSua = "GetlistInNhanTraSua";
    public const string GetDataInTemTraSua = "GetDataInTemTraSua";
    public const string UpdateStatusInNhan = "UpdateStatusInNhan";
    public const string GetDataTienDauCa = "GetDataTienDauCa";
    public const string SaveDataTienDauCa = "SaveDataTienDauCa";
    public const string GetDataTienKetThucCa = "GetDataTienKetThucCa";
    public const string SaveDataTienKetThucCa = "SaveDataTienKetThucCa";
    public const string DeleteInvoiceTmp = "DeleteInvoiceTmp";
    public const string TimHangHoaTraHang = "TimHangHoaTraHang";
    public const string GetCodeNameDataMaster = "GetCodeNameDataMaster";
    public const string GetDataInPhieuTraHang = "GetDataInPhieuTraHang";
    public const string GetDetailById = "GetDetailById";
    public const string GetDetailBangGiaLePosById = "GetDetailBangGiaLePosById";
    public const string SyncDataMaster = "SyncDataMaster";
    public const string SyncDataPromotion = "SyncDataPromotion";
    public const string SyncDataUser = "SyncDataUser";
    public const string SyncData_CompanyInfo = "SyncData_CompanyInfo";
    public const string SyncDataSaleInvoice_ClientToServer = "SyncDataSaleInvoice_ClientToServer";
    public const string SyncDataSaleInvoiceTmp_ClientToServer = "SyncDataSaleInvoiceTmp_ClientToServer";
    public const string SyncDataDeleteTmp_ClientToServer = "SyncDataDeleteTmp_ClientToServer";
    public const string SaveInvoiceTmp_Info = "SaveInvoiceTmp_Info";
    public const string Syncdata_GetDataSaleInvoiceTmp = "Syncdata_GetDataSaleInvoiceTmp";
    public const string GhiNhanTinhGio = "GhiNhanTinhGio";
    public const string TinhTienGio = "TinhTienGio";
    public const string TinhTienGio_TheoThoiDiem = "TinhTienGio_TheoThoiDiem";
    public const string GetLogTinhTienGio = "GetLogTinhTienGio";
    public const string Search = "Search";
    public const string Save = "Save";
    public const string Deletelist = "Deletelist";
    public const string GetDataCodeName = "GetDataCodeName";
    public const string detail = "detail";
    public const string DoiDiemTheVip = "DoiDiemTheVip";
    public const string BaoCaoDoanhSoTheoNhanVien = "BaoCaoDoanhSoTheoNhanVien";
    public const string BaoCaoDoanhSoTheoKhuVuc = "BaoCaoDoanhSoTheoKhuVuc";
    public const string BaoCaoDoanhThuChiTietTheoNhomHang = "BaoCaoDoanhThuChiTietTheoNhomHang";
    public const string GetLogItemOrder = "GetLogItemOrder";
    public const string LogOff = "LogOff";
}


		public struct Language
{
    public const string VN = "vi-VN";
    public const string EN = "en-US";
}

public struct eLoginType
{
    public const int Online = 0;
    public const int Offline = 1;
}

	}
}
