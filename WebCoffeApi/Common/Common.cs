using System;

namespace FasCon.Apis.Common
{
    public class Common
    {
        public static string DisplayMessage(int iReturnCode)
        {
            string result = string.Empty;
            switch (iReturnCode)
            {
                case 0:
                    result = "CMN_MSG_SAVE_SUCESSFULLY";
                    break;
                case 1:
                    result = "CMN_MSG_ACCESS_DENIED";
                    break;
                case 2:
                    result = "CMN_MSG_INVALID_HOST";
                    break;
                case 3:
                    result = "CMN_MSG_INVALID_DATABASE";
                    break;
                case 4:
                    result = "CMN_MSG_LOST_CONNECTION";
                    break;
                case 5:
                    result = "CMN_MSG_DUPLICATE_DATA";
                    break;
                case 6:
                    result = "CMN_MSG_FORGEIGN_NOT_EXIST";
                    break;
                case 7:
                    result = "CMN_MSG_FORGEIGN_KEY_VIOLATION";
                    break;
                case 8:
                    result = "CMN_MSG_DATA_NOT_FOUND";
                    break;
                case 9:
                    result = "CMN_MSG_EXCEPTION_OCCURED";
                    break;
                case 10:
                    result = "CMN_MSG_DELETE_CURRENT_USER";
                    break;
                case 11:
                    result = "CMN_MSG_DUPLICATE_DOCUMENT_NO";
                    break;
                case 12:
                    result = "CMN_MSG_NOT_PERMISSION";
                    break;
            }
            return result;
        }

        public static string getDayOfWeeks(int dayofweek)
        {
            string empty = string.Empty;
            return arrDayOfWeeks[dayofweek];
        }

        private static string[] arrDayOfWeeks = new string[]
        {
            "Sun",
            "Mon",
            "Tue",
            "Wed",
            "Thu",
            "Fri",
            "Sat"
        };

        public struct DbReturnCode
        {
            public const int Succeed = 0;
            public const int AccessDenied = 1;
            public const int InvalidHost = 2;
            public const int InvalidDatabase = 3;
            public const int LostConnection = 4;
            public const int DuplicateKey = 5;
            public const int ForgeignKeyNotExist = 6;
            public const int ForeignKeyViolation = 7;
            public const int DataNotFound = 8;
            public const int ExceptionOccured = 9;
            public const int DeleteCurrentUser = 10;
            public const int DuplicateDocumentNo = 11;
            public const int UserNotPermissionAction = 12;
            public const int FileNotExist = 13;
            public const int NotLicense = 14;
            public const int ServiceUnavailable = 503;
        }

        public struct LoaiHinhKinhDoanh
        {
            public const string NhaHang_Cafe = "001";
            public const string TraSua = "002";
            public const string BanLe = "003";
            public const string Karaoke = "004";
        }

        public struct MySqlErrorNumber
        {
            public const int AccessDenied = 1045;
            public const int DuplicateKey = 1062;
            public const int ForgeignKeyNotExist = 1452;
            public const int ForgeignKeyViolation = 1451;
            public const int InvalidDatabase = 1049;
            public const int InvalidHost = 2005;
            public const int LostConnection = 1042;
        }

        public struct DefaultValue
        {
            public const string LocationDefault = "LocationDefault";
        }

        public struct EPriceNo
        {
            public const string Price1 = "Price1";
            public const string Price2 = "Price2";
            public const string Price3 = "Price3";
            public const string Price4 = "Price4";
            public const string Price5 = "Price5";
            public const string Price6 = "Price6";
            public const string Price7 = "Price7";
            public const string Price8 = "Price8";
            public const string Price9 = "Price9";
            public const string Price10 = "Price10";
            public const string Price11 = "Price11";
            public const string Price12 = "Price12";
            public const string Price13 = "Price13";
            public const string Price14 = "Price14";
            public const string Price15 = "Price15";
            public const string Price16 = "Price16";
            public const string Price17 = "Price17";
            public const string Price18 = "Price18";
            public const string Price19 = "Price19";
            public const string Price20 = "Price20";
        }

        public struct CounterPrimaryKey
        {
            public const string PhieuNhapKho = "PNK";
            public const string PhieuXuatKho = "PXK";
            public const string PhieuChuyenKho = "PCK";
            public const string HoaDonBanSi = "HDS";
            public const string HoaDonBanLe = "HDL";
            public const string HoaDonBanTraHang = "PNT";
            public const string DonDatHangBan = "DHB";
            public const string PhieuChiTien = "PC";
            public const string PhieuThuTien = "PT";
            public const string PromotionDeal = "CTKM";
        }

        public struct StatusType
        {
            public const int Active = 1;
            public const int InActive = 0;
        }

        public struct ScreenCode
        {
            public const string ItemCategory = "ItemCategory";
            public const string Item = "Item";
            public const string Cost = "Cost";
            public const string Customer = "Customer";
            public const string CustomerCategory = "CustomerCategory";
            public const string Department = "Department";
            public const string Employee = "Employee";
            public const string LocationStore = "LocationStore";
            public const string PriceList = "PriceList";
            public const string RelationPrice = "RelationPrice";
            public const string SaleArea = "SaleArea";
            public const string SaleTable = "SaleTable";
            public const string Supplier = "Supplier";
            public const string SupplierCategory = "SupplierCategory";
            public const string TimeOfSale = "TimeOfSale";
            public const string UnitOfMeasure = "UnitOfMeasure";
            public const string Restaurant = "Restaurant";
            public const string RestaurantList = "RestaurantList";
            public const string Promotion = "Promotion";
            public const string StockIn = "StockIn";
            public const string StockOut = "StockOut";
            public const string StockTransfer = "StockTransfer";
            public const string InvDinhMuc = "InvDinhMuc";
            public const string Sales_DoanhSoNhomTheoNhanVien = "Sales_DoanhSoNhomTheoNhanVien";
            public const string Sales_ChiTietBanHang_TheoNhomHang = "Sales_ChiTietBanHang_TheoNhomHang";
            public const string Sales_ChiTietBanHang_TheoKhuVuc = "Sales_ChiTietBanHang_TheoKhuVuc";
            public const string Inv_BC_TonKho = "Inv_BC_TonKho";
            public const string Inv_BC_PhieuNhapKho = "Inv_BC_PhieuNhapKho";
            public const string Inv_BC_PhieuXuatKho = "Inv_BC_PhieuXuatKho";
            public const string Inv_BC_PhieuChuyenKho = "Inv_BC_PhieuChuyenKho";
            public const string Inv_BC_BangKeThanhToan = "Inv_BC_BangKeThanhToan";
            public const string Inv_BC_BangKeChiTiet_TheoHoaDon = "Inv_BC_BangKeChiTiet_TheoHoaDon";
            public const string Inv_BC_HoaDonBanHang_TheoKhachHang = "Inv_BC_HoaDonBanHang_TheoKhachHang";
            public const string Inv_BC_HoaDonBanHang_TheoNhanVien = "Inv_BC_HoaDonBanHang_TheoNhanVien";
            public const string Inv_BC_HoaDonBanHang_TheoKhuVuc = "Inv_BC_HoaDonBanHang_TheoKhuVuc";
            public const string Inv_BC_HoaDonBanHang_ChiTiet = "Inv_BC_HoaDonBanHang_ChiTiet";
            public const string Inv_BC_DonHangMuaHang_TheoNhaCungCap = "Inv_BC_DonHangMuaHang_TheoNhaCungCap";
            public const string Inv_BC_DonHangMuaHang_TheoNhanVien = "Inv_BC_DonHangMuaHang_TheoNhanVien";
            public const string Inv_BC_DonHangMuaHang_ChiTiet = "Inv_BC_DonHangMuaHang_ChiTiet";
            public const string Inv_BC_ChiTietNhapKho_TheoPhieuNhap = "Inv_BC_ChiTietNhapKho_TheoPhieuNhap";
            public const string Inv_BC_ChiTietXuatKho_TheoPhieuXuat = "Inv_BC_ChiTietXuatKho_TheoPhieuXuat";
            public const string Inv_BC_ChiTietChuyenKho_TheoPhieuChuyenKho = "Inv_BC_ChiTietChuyenKho_TheoPhieuChuyenKho";
            public const string Inv_BC_TongHop = "Inv_BC_TongHop";
            public const string Sale_BC_TongHop = "Sale_BC_TongHop";
            public const string Sale_BC_TheoKhuVuc = "Sale_BC_TheoKhuVuc";
            public const string Sale_BC_ChiTiet = "Sale_BC_ChiTiet";
            public const string Sale_BC_DoanhSoChiTiet_TheoNhanVien = "Sale_BC_DoanhSoChiTiet_TheoNhanVien";
            public const string Sale_BC_DoanhSoChiTiet_TheoKhuVuc = "Sale_BC_DoanhSoChiTiet_TheoKhuVuc";
            public const string Sale_BC_DoanhSoTongHop_TheoNhanVien = "Sale_BC_DoanhSoTongHop_TheoNhanVien";
            public const string Sale_BC_DoanhSoTongHop_TheoKhuVuc = "Sale_BC_DoanhSoTongHop_TheoKhuVuc";
            public const string Sale_BC_CongNoKhachHang = "Sale_BC_CongNoKhachHang";
            public const string Sale_BC_CongNoNhaCungCap = "Sale_BC_CongNoNhaCungCap";
            public const string Sale_BC_CongNoNhomHang = "Sale_BC_CongNoNhomHang";
            public const string Sale_BC_CongNoKhachHangTheoNhom = "Sale_BC_CongNoKhachHangTheoNhom";
            public const string Sale_BC_CongNoNhaCungCapTheoNhom = "Sale_BC_CongNoNhaCungCapTheoNhom";
            public const string Sale_BC_CongNoTheoKhachHang = "Sale_BC_CongNoTheoKhachHang";
            public const string Sale_BC_CongNoTheoNhaCungCap = "Sale_BC_CongNoTheoNhaCungCap";
            public const string Sale_BC_ThuChiTheoNhanVien = "Sale_BC_ThuChiTheoNhanVien";
            public const string Sale_BC_ThuChiTheoKhuVuc = "Sale_BC_ThuChiTheoKhuVuc";
            public const string Sale_BC_BaoCaoDoanhSoTongHopTheoThang = "Sale_BC_BaoCaoDoanhSoTongHopTheoThang";
            public const string Sale_BC_BaoCaoDoanhSoChiTietTheoThang = "Sale_BC_BaoCaoDoanhSoChiTietTheoThang";
            public const string Sale_BC_BaoCaoDoanhSoTongHopTheoNam = "Sale_BC_BaoCaoDoanhSoTongHopTheoNam";
            public const string Sale_BC_BaoCaoDoanhSoChiTietTheoNam = "Sale_BC_BaoCaoDoanhSoChiTietTheoNam";
            public const string Sale_BC_BaoCaoThuChiTheoThang = "Sale_BC_BaoCaoThuChiTheoThang";
            public const string Sale_BC_BaoCaoThuChiTheoNam = "Sale_BC_BaoCaoThuChiTheoNam";
        }
    }
}
