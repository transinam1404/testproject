using System;
using System.Collections.Generic;
using System.Data;

namespace FasCon.Apis.Patterns
{
	public interface IUnitOfWork : IDisposable
	{
		void BeginTransaction(IsolationLevel isoLevel = IsolationLevel.ReadCommitted);

		int CommitTransaction();

		void RollBackTransaction();

		IEnumerable<TEntity> Query<TEntity>(string query, params object[] parameters);

		TEntity QueryFirstOrDefault<TEntity>(string query, params object[] parameters);
	}
}
